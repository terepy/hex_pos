use hex_pos::Pos;

#[test]
fn test_to_world_corners() {
    let pos = Pos { x: 1, q: 1 };
    let corners = pos.to_world_corners();
    
    // Expected corners for the given position
    let expected_corners = [
        (2.0, -1.1547005),
        (1.5, -1.4433756),
        (1.0, -1.1547005),
        (1.0, -0.57735026),
        (1.5, -0.28867513),
        (2.0, -0.57735026),
    ];

    for (corner, expected) in corners.iter().zip(expected_corners.iter()) {
        println!("({:.2}, {:.2}) vs ({:.2}, {:.2})",corner.0, corner.1, expected.0, expected.1);
    }

    for (corner, expected) in corners.iter().zip(expected_corners.iter()) {
        assert!((corner.0 - expected.0).abs() < 1e-6);
        assert!((corner.1 - expected.1).abs() < 1e-6);
    }
}
