#[cfg(feature="serde")]
use serde::{Serialize,Deserialize};

use std::ops::*;
use std::fmt;

//pointy topped hexagonal coordinates represented axially, with x corrsponding to the rightwards direction and q corresponding to up-right
//assumes y axis points downwards in world coordinates
#[repr(C)]
#[cfg_attr(feature="serde", derive(Serialize,Deserialize))]
#[derive(Default,Debug,Copy,Clone,Eq,PartialEq,Ord,PartialOrd,Hash)]
pub struct Pos {
	//corresponds to positive x in cartesion coordinates
	pub x: i32,
	//corresponds to (0.5, -sqrt(3)/2)
	pub q: i32,
}

pub fn hex(x: i32, q: i32) -> Pos {
	Pos { x, q, }
}

pub fn range(a: Pos, b: Pos) -> i32 {
	a.range_to(b)
}

pub fn angle(a: Pos, b: Pos) -> f32 {
	a.angle_to(b).abs()
}

impl Pos {
	pub fn to_world(self) -> (f32, f32) {
		(self.x as f32 + 0.5 * self.q as f32, -(3f32.sqrt()) / 2.0 * self.q as f32)
	}
	
	pub fn to_world_corners(self) -> [(f32, f32); 6] {
		hex_corners(self.to_world(), 1.0)
	}
	
	pub fn from_world(x: f32, y: f32) -> Self {
		//convert to cube coords, correct one, convert back to axial
		let x = x + y / 3f32.sqrt();
		let q = -2.0 / 3f32.sqrt() * y;
		let s = -x-q;

		let dq = (q.round() - q).abs();
		let dx = (x.round() - x).abs();
		let ds = (s.round() - s).abs();
		let mut x = x.round() as i32;
		let mut q = q.round() as i32;
		let s = s.round() as i32;

		if dq > dx && dq > ds {
			q = -x-s;
		} else if dx > ds {
			x = -q-s;
		}
		hex(x, q)
	}
	
	pub fn range_to(self, other: Self) -> i32 {
		let (x, q) = (self - other).into();
		(x.abs() + q.abs() + (x+q).abs()) / 2
	}

	pub fn angle_to(self, other: Self) -> f32 {
		let (x, y) = (self - other).to_world();
		(-y).atan2(x) //y inverted here because y axis points downwards
	}

	pub fn in_range(self, radius: i32) -> RangeIter {
		RangeIter::new(self, radius)
	}
	
	pub fn on_radius(self, radius: i32) -> RadiusIter {
		RadiusIter::new(self, radius)
	}
	
	pub fn adjacent(self) -> [Pos; 6] {
		[Right,UpRight,UpLeft,Left,DownLeft,DownRight].map(|dir| self + dir)
	}
	
	pub fn dir_to(self, other: Self) -> Dir {
		let d = self - other;
		let a = d.x * 2 + d.q;
		let b = 3 * d.q;
		match (a > 0, b > a, b < -a) {
			(true, true, _) => UpRight,
			(true, false, true) => DownRight,
			(true, false, false) => Right,
			(false, false, _) => DownLeft,
			(false, true, true) => Left,
			(false, true, false) => UpLeft,
		}
	}
	
	//assumes that self.range_to(other) == 1, always returns right otherwise
	pub fn dir_to_adj(self, other: Pos) -> Dir {
		let d = other - self;
		match (d.x, d.q) {
			(0,1) => UpRight,
			(-1,1) => UpLeft,
			(-1,0) => Left,
			(0,-1) => DownLeft,
			(1,-1) => DownRight,
			_ => Right,
		}
	}
	
	pub fn rotate_by(self, amount: usize) -> Self {
		let matrix = match amount % 6 {
			0 => [[1, 0], [0, 1]],
			1 => [[0, -1], [1, 1]],
			2 => [[-1, -1], [1, 0]],
			3 => [[-1, 0], [0, -1]],
			4 => [[0, 1], [-1, -1]],
			_ => [[1, 1], [-1, 0]],
		};
		hex(matrix[0][0] * self.x + matrix[0][1] * self.q, matrix[1][0] * self.x + matrix[1][1] * self.q)
	}

	//element-wise min
	pub fn min(self, other: Self) -> Self {
		hex(self.x.min(other.x), self.q.min(other.q))
	}
	
	//element-wise max
	pub fn max(self, other: Self) -> Self {
		hex(self.x.max(other.x), self.q.max(other.q))
	}
	
	//element-wise clamp
	pub fn clamp(self, min: Self, max: Self) -> Self {
		hex(self.x.clamp(min.x, max.x), self.q.clamp(min.q, max.q))
	}
	
}

impl fmt::Display for Pos {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "({} {})", self.x, self.q)
	}
}

impl From<Pos> for (i32, i32) {
	fn from(p: Pos) -> Self {
		(p.x, p.q)
	}
}

impl From<Pos> for [i32; 2] {
	fn from(p: Pos) -> Self {
		[p.x, p.q]
	}
}

impl Neg for Pos {
	type Output = Self;
	fn neg(self) -> Self {
		hex(-self.x, -self.q)
	}
}

impl Add<Pos> for Pos {
	type Output = Self;
	fn add(self, other: Self) -> Self {
		hex(self.x + other.x, self.q + other.q)
	}
}

impl Sub<Pos> for Pos {
	type Output = Self;
	fn sub(self, other: Self) -> Self {
		hex(self.x - other.x, self.q - other.q)
	}
}

impl Add<Dir> for Pos {
	type Output = Self;
	fn add(self, dir: Dir) -> Self {
		self + Self::from(dir)
	}
}

impl Sub<Dir> for Pos {
	type Output = Self;
	fn sub(self, dir: Dir) -> Self {
		self - Self::from(dir)
	}
}

impl AddAssign<Pos> for Pos {
	fn add_assign(&mut self, other: Self) {
		*self = *self + other;
	}
}

impl SubAssign<Pos> for Pos {
	fn sub_assign(&mut self, other: Self) {
		*self = *self - other;
	}
}

impl AddAssign<Dir> for Pos {
	fn add_assign(&mut self, dir: Dir) {
		*self += Self::from(dir);
	}
}

impl SubAssign<Dir> for Pos {
	fn sub_assign(&mut self, dir: Dir) {
		*self -= Self::from(dir);
	}
}

impl Mul<i32> for Pos {
	type Output = Self;
	fn mul(self, scalar: i32) -> Self {
		hex(self.x * scalar, self.q * scalar)
	}
}

impl Div<i32> for Pos {
	type Output = Self;
	fn div(self, scalar: i32) -> Self {
		hex(self.x / scalar, self.q / scalar)
	}
}

#[repr(u8)]
#[cfg_attr(feature="serde", derive(Serialize,Deserialize))]
#[derive(Debug,Copy,Clone,Eq,PartialEq,Hash)]
pub enum Dir {
	Right = 0,
	UpRight = 1,
	UpLeft = 2,
	Left = 3,
	DownLeft = 4,
	DownRight = 5,
}
pub use Dir::*;

impl Dir {
	pub fn left(self) -> Self {
		(self as u8 + 1).into()
	}
	
	pub fn right(self) -> Self {
		(self as u8 + 5).into()
	}

	pub fn rotate_by(self, amount: u8) -> Self {
		(self as u8 + amount).into()
	}
	
	pub fn to_radians(self) -> f32 {
		self as u8 as f32 * std::f32::consts::FRAC_PI_3
	}

	pub fn to_world(self) -> (f32, f32) {
		Pos::from(self).to_world()
	}
}

impl From<u8> for Dir {
	fn from(i: u8) -> Self {
		[Right,UpRight,UpLeft,Left,DownLeft,DownRight][i as usize % 6]
	}
}

impl From<Dir> for Pos {
	fn from(d: Dir) -> Self {
		match d {
			Right => hex(1,0),
			UpRight => hex(0,1),
			UpLeft => hex(-1,1),
			Left => hex(-1,0),
			DownLeft => hex(0,-1),
			DownRight => hex(1,-1),
		}
	}
}

impl Neg for Dir {
	type Output = Self;
	fn neg(self) -> Self {
		((self as u8) + 3).into()
	}
}

impl fmt::Display for Dir {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let c = ["→","↗","↖","←","↙","↘"][*self as usize];
		f.write_str(c)
	}
}

#[derive(Debug,Copy,Clone)]
pub struct RangeIter {
	pub center: Pos,
	pub current: Pos,
	pub radius: i32,
}

impl RangeIter {
	pub fn new(center: Pos, radius: i32) -> Self {
		Self {
			center,
			current: hex(-radius, 0),
			radius,
		}
	}
}

impl Iterator for RangeIter {
	type Item = Pos;
	//x in (-r)..=r, q in (-r)..=r gives a rhombus
	//we need to cut off the sides of the rhombus to get a hexagon
	//x in (-r)..=r, q in (-r).max(-r-x)..=r.min(r-x)
	fn next(&mut self) -> Option<Pos> {
		if self.current.x > self.radius {
			None
		} else {
			let r = Some(self.center + self.current);
			self.current.q += 1;
			if self.current.q > self.radius.min(self.radius - self.current.x) {
				self.current.x += 1;
				self.current.q = -self.radius.min(self.radius + self.current.x);
			}
			r
		}
	}
}

#[derive(Debug,Copy,Clone)]
pub struct RadiusIter {
	pub current: Pos,
	pub dir: Dir,
	pub i: i32,
	pub radius: i32,
}

impl RadiusIter {
	pub fn new(center: Pos, radius: i32) -> Self {
		Self {
			current: center + hex(radius,-radius),
			dir: Left,
			i: 0,
			radius,
		}
	}
}

impl Iterator for RadiusIter {
	type Item = Pos;
	//we trace a loop by repeatedly adding directions
	fn next(&mut self) -> Option<Pos> {
		if self.i >= self.radius * 6 {
			None
		} else {
			let r = Some(self.current);
			self.current += self.dir;
			self.i += 1;
			if self.i % self.radius == 0 {
				self.dir = self.dir.right();
			}
			r
		}
	}
	
	fn size_hint(&self) -> (usize, Option<usize>) {
		let s = (self.radius * 6 - self.i) as usize;
		(s, Some(s))
	}
}

pub fn hex_corners(center: (f32, f32), scale: f32) -> [(f32, f32); 6] {
	let scale = scale / 3.0;
	let x = [1f32, 0.0, -1.0, -1.0, 0.0, 1.0];
	let q = [0.0, 1.0, 1.0, 0.0, -1.0, -1.0];
    [0,1,2,3,4,5].map(|i| {
		let q = q[i] + q[(i+1)%6];
		let x = x[i] + x[(i+1)%6] + 0.5 * q;
		let y = -(3f32.sqrt()) / 2.0 * q;
		(center.0 + x * scale, center.1 + y * scale)
    })
}
