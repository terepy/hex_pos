This crate implements hexagonal positions.  
it uses axial coordinates x and q, with x pointing rightwards (same as x in cartesians) and q pointing up-right (+0.5x -sqrt(3)/2*y, assumes y axis points downwards).  
